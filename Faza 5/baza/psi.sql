-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2015 at 10:22 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `psi`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `UserName` char(25) NOT NULL,
  `password` char(25) DEFAULT NULL,
  `Ime` char(25) DEFAULT NULL,
  `Prezime` char(25) DEFAULT NULL,
  `Adresa` char(25) DEFAULT NULL,
  `Grad` char(25) DEFAULT NULL,
  `Email` char(25) DEFAULT NULL,
  `Telefon` varchar(15) NOT NULL,
  `isAdmin` int(11) NOT NULL DEFAULT '0',
  `isModerator` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`UserName`, `password`, `Ime`, `Prezime`, `Adresa`, `Grad`, `Email`, `Telefon`, `isAdmin`, `isModerator`) VALUES
('admin', 'admin', 'Admin', 'Admin', 'Admin', 'Admin', 'Admin', 'Admin', 1, 0),
('moderator', 'moderator', 'Moderator', 'Moderator', 'Moderator', 'Moderator', 'Moderator', 'Moderator', 0, 1),
('Nenad', 'nenad', 'Nenad', 'Rajic', 'Ruzveltova', 'Beograd', 'rajkerpn@yahoo.com', '1234566', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `korpa`
--

CREATE TABLE IF NOT EXISTS `korpa` (
  `Cena` float DEFAULT NULL,
  `IDArtikla` int(5) DEFAULT NULL,
  `UserName` varchar(25) DEFAULT NULL,
  `Kolicina` int(11) NOT NULL DEFAULT '1',
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korpa`
--

INSERT INTO `korpa` (`Cena`, `IDArtikla`, `UserName`, `Kolicina`) VALUES
(3.95, 3, 'Milutin', 1),
(0.6, 19, 'Milutin', 1),
(4, 27, 'Milutin', 1),
(2.777, 29, 'Milutin', 1),
(1.2, 31, 'Milutin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `proizvod`
--

CREATE TABLE IF NOT EXISTS `proizvod` (
  `IDArtikla` int(5) NOT NULL AUTO_INCREMENT,
  `Naziv` char(25) DEFAULT NULL,
  `Stanje` int(11) DEFAULT NULL,
  `Kategorija` char(25) DEFAULT NULL,
  `Proizvodjac` char(25) DEFAULT NULL,
  `Opis` varchar(200) DEFAULT NULL,
  `Cena` float DEFAULT NULL,
  `Slika` varchar(50) NOT NULL,
  `isAkcija` int(1) NOT NULL DEFAULT '0',
  `procAkcija` int(3) DEFAULT NULL,
  PRIMARY KEY (`IDArtikla`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `proizvod`
--

INSERT INTO `proizvod` (`IDArtikla`, `Naziv`, `Stanje`, `Kategorija`, `Proizvodjac`, `Opis`, `Cena`, `Slika`, `isAkcija`, `procAkcija`) VALUES
(1, ' GEFORCE 8400GS', 100, 'GrafickeKarte', 'Nvidia', '512MB 64BIT PCI Express', 3.5, 'GIGABYTE NVIDIA GEFORCE 8400GS.jpg', 1, 20),
(2, 'GEFORCE 210', 50, 'GrafickeKarte', 'Nvidia', '1GB 64BIT PCI Express', 3.65, 'GEFORCE 210.jpg', 0, NULL),
(3, 'Radeon HD5450', 20, 'GrafickeKarte', 'Nvidia', '1024MB DDR3 PCI Express', 3.95, 'Radeon HD5450.jpg', 0, NULL),
(4, 'GeForce GT520', 5, 'GrafickeKarte', 'Nvidia', '1024MB 64 bit PCI Express', 4.1, 'GeForce GT520.jpg', 0, NULL),
(5, 'GeForce GT520', 25, 'GrafickeKarte', 'Nvidia', '1024MB 64 bit PCI Express', 5.65, 'GeForce GT520.jpg', 0, NULL),
(7, 'Asus GeForce GT520', 5, 'GrafickeKarte', 'Nvidia', '512MB 32bit PCI Express', 5.88, 'Asus GeForce GT520.jpg', 0, NULL),
(8, 'GeForce GT220', 12, 'GrafickeKarte', 'Nvidia', '1024MB 64bit PCI Express', 5.99, 'GeForce GT220.jpg', 1, 20),
(9, 'GeForce GT430', 17, 'GrafickeKarte', 'Nvidia', '1024MB 64bit PCI Express', 6.54, 'GeForce GT430.jpg', 0, NULL),
(11, 'GeForce GT440', 500, 'GrafickeKarte', 'Nvidia', '1024MB 128bit PCI Express', 6.67, 'GeForce GT440.jpg', 0, NULL),
(12, 'LG28LB450B', 100, 'Televizori', 'LG', '28" - 71cm, LED- HDMI , SCART , USB...', 30, 'LG28LB450B.jpg', 0, NULL),
(13, 'HP TOUCHSMART ELITE 7320', 1000, 'Laptopovi', 'Intel', 'IntelÂ® Coreâ„¢ i3-2120', 60, 'HP TouchSmart.jpg', 0, NULL),
(17, 'WD 250GB 2.5"', 1000, 'HardDiskovi', 'Western Digital', 'SATA II, 8MB, 5400RPM', 2.255, 'WD 250.jpg', 0, NULL),
(18, 'WD 500GB 2.5"', 555, 'HardDiskovi', 'Western Digital', 'SATA II, 8MB, 5400RPM', 6.25, 'WD 500.jpg', 0, NULL),
(19, 'Intex HDD Rack', 600, 'HDDRack', 'Western Digital', 'Eksterno USB3.0 HDD ', 0.6, 'Intex HDD Rack.jpg', 0, NULL),
(20, 'LC POWER HDD RACK', 555, 'HDDRack', 'Nvidia', 'Eksterno USB3.0 HDD ', 0.9, 'LC POWER HDD RACK.jpg', 0, NULL),
(22, 'HR 159 0F CL', 50, 'Frizideri', 'Bira', 'Energetska klasa: A, Bruto zapremina: 130 l', 18, 'frizider1.jpg', 0, NULL),
(23, 'R6150BX', 50, 'Frizideri', 'Gorenje', 'Energetski razred: A, Bruto zapremina: 305 l ', 35, 'frizider2.jpg', 0, NULL),
(24, 'Cooler Master  S478', 55555, 'Kuleri', 'Nvidia', ' S478', 1.5, 'Cooler Master s478.jpg', 0, NULL),
(26, 'Cooler Master Hyper TX3', 555, 'Kuleri', 'Nvidia', 'CPU', 5.254, 'Cooler MasterHyper.jpg', 0, NULL),
(27, 'Asus P5G41T-M', 2222, 'MaticnePloce', 'Asus', 'Intel Socket 775', 4, 'Asus P5G41T-M.jpg', 0, NULL),
(28, 'MSI 760GM-P21', 2222, 'MaticnePloce', 'MSI', 'AMD Socket AM3+', 4.5, 'MSI 760GM-P21.jpg', 0, NULL),
(29, 'Kingston SO-DIMM 2GB', 5555, 'Memorija', 'Nvidia', 'SODIMM, 2GB, 1333 MHz', 2.777, 'Kingston SO-DIMM 2GB.jpg', 0, NULL),
(30, 'Kingston 1 GB DDR2 HyperX', 55555, 'Memorija', 'Nvidia', '1GB, 1066 MHz', 4.5, 'Kingston 1 GB DDR2 HyperX.jpg', 0, NULL),
(31, 'Logitech X-scroll', 3333, 'Misevi', 'Logitech', 'PS/2', 1.2, 'Genius X-scroll.jpg', 0, NULL),
(32, 'Logitech Netscroll 100', 2222, 'Misevi', 'Logitech', 'PS/2', 2, 'Genius Netscroll 100.jpg', 0, NULL),
(33, 'Asus TFT 18.5"', 6666, 'Monitori', 'Asus', '18.5" dijagonala,1366x768', 15, 'Asus TFT 18.5.jpg', 0, NULL),
(34, 'Asus LED TFT 19"', 7777, 'Monitori', 'Asus', '19" dijagonala,1366x768', 20, 'Asus LED TFT 19.jpg', 0, NULL),
(35, 'Legria HFR 606 BK', 10, 'Kamere', 'Canon', '3,28 megapiksela, Opticki Zum: 32x, Napredni zum: 57x', 36.5, 'kamera1.jpg', 0, NULL),
(36, 'DSLR EOS 1200D', 20, 'Fotoaparati', 'Canon', 'EF-S 10-22mm \n18 megapiksela\nTFT 7,5 cm (3")', 50, 'canon1.jpg', 0, NULL),
(37, 'HMX-W300YP ORANGE', 10, 'Kamere', 'Samsung', 'Gumirani dÅ¾epni kamkorder , 5M BSI CMOS senzor ', 16, 'kamera2.jpg', 0, NULL),
(38, 'KAS 35', 5, 'Klime', 'Gorenje', ' Rashladna snaga: 3500 W,\r\nGrejna snaga: 3660 W ', 34, 'klima1.jpg', 0, NULL),
(39, 'pokretna KAM 26', 7, 'Klime', 'Gorenje', 'Energetski razred: A ,\r\n2.6 kw 9000 BTU', 32, 'klima2.jpg', 0, NULL),
(40, 'C18AHR', 2, 'Klime', 'LG', 'NEO-Plasma tehnologija,\r\n18000 BTU, HlaÄ‘enje: 1750 W', 100.5, 'klima3.jpg', 0, NULL),
(41, 'MacBook Air MD760Z/B', 5, 'Laptopovi', 'Apple', '13.3" LED (1440x900) â€¢ Intel i5 Dual-core, 1.4GHz ', 132, 'laptopovi1.jpg', 0, NULL),
(42, 'GS52214W', 10, 'MasineZaSudove', 'Gorenje', 'Kapacitet 9 kompleta posuÄ‘a â€¢ Energetski razred A+ ', 33, 'masinaZasudove1.jpg', 0, NULL),
(43, 'GS64314XS', 5, 'MasineZaSudove', 'Gorenje', 'Energetski razred A++ â€¢  Kapacitet standardni set za 14 ', 48, 'masinaZasudove2.jpg', 0, NULL),
(44, 'W6202S - Slim', 5, 'MasineZaVes', 'Gorenje', 'Energetski razred A++ â€¢ Kapacitet do 6,5kg â€¢ Eco Sistem ', 30, 'vesMasina1.jpg', 0, NULL),
(45, 'F 1443KDS', 2, 'MasineZaVes', 'LG', ' Energetska klasa A+++ â€¢ Kapacitet 11 kg ', 100, 'vesMasine2.jpg', 0, NULL),
(46, 'WF 60F4E5W2X - Slim', 4, 'MasineZaVes', 'Samsung', 'Energetski razred A++â€¢ Kapacitet 6 kg ', 65, 'vesMasina3.jpg', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vesti`
--

CREATE TABLE IF NOT EXISTS `vesti` (
  `IDVesti` int(3) NOT NULL AUTO_INCREMENT,
  `Naslov` char(25) DEFAULT NULL,
  `Opis` text,
  `Kategorija` char(25) DEFAULT NULL,
  PRIMARY KEY (`IDVesti`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `vesti`
--

INSERT INTO `vesti` (`IDVesti`, `Naslov`, `Opis`, `Kategorija`) VALUES
(6, 'Apple iPhone 6', '\n\nIn creating iPhone 6, we scrutinized every element and material. Thatâ€™s how we arrived at a smooth, continuous form. A thinner profile made possible by our thinnest display yet. And intuitively placed buttons. All made with beautiful anodized aluminum, stainless steel, and glass. Itâ€™s a thousand tiny details that add up to something big. Or in this case, two big things: iPhone 6 and iPhone 6 Plus.\n', 'Mobilni Telefoni');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
