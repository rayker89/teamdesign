﻿<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main" >
					<label class="termn"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pravila korišćenja</p></label></br></br>
					<label class="term">Da bi Vam bila omogućena kupovina na našem sajtu morate biti registrovani korisnik. 
					Proces registracije je veoma jednostavan i od Vas zahteva samo osnovne lične podatke.
					Prilikom pregleda proizvoda primetićete da pored svakog posebno postoji dugme "stavi u korpu". 
					Kada kliknte to dugme bićete obavešteni da se proizvod nalazi u Vašoj korpi.
					Moguće je više puta dodati isti proizvod u korpu.
					U svakom trenutku omogućen Vam je pregled svih proizvoda koje želite da kupite. 
					Jednos tavnim klikom na ikonicu korpe u gornjem desnom uglu sajta otvarate stranicu na kojoj su prikazani svi proizvodi spremni za kupovinu.
					Veoma lako možete obrisati proizvode iz korpe ili povećati broj željenih proizvoda.
					Kada ste spremni da izvršite kupovinu potrebno je samo da na stranici korpa kliknete dugme "naruči robu iz korpe". 
					Ukoliko ste se predomislili u vezi kupovine i želite nešto da promenite jednostavno se možete vratiti na stranicu korpa klikom na dugme "odustani".
					Sve cene na ovom sajtu iskazane su u dinarima. PDV je uračunat u cenu.
					Prodavnica kompjutera ne garantuje da su sve informacije i fotografije artikala na ovom sajtu u potpunosti ispravne.
					Svi artikli prikazani na ovom sajtu su deo naše ponude i ne podrazumeva se da su svi dostupni u svakom trenutku na našem lageru.>
					Raspoloživost artikala možete proveriti i slanjem upita na e-mail.
				</label>	
			</div>
			</div>
         <?php require_once("footer.php");?>
	</div>
</body>
</html>

