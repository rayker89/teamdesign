﻿<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main" >
				<div class="help">
				<p><label class="helpn">Prijavljivanje</label>

<br>Ukoliko niste prijavljeni, moraćete se prijaviti ili se registrovati ukoliko niste do sada kupovali. Registracija je brza, jednostavna i besplatna.</p>
				<p><label class="helpn">Pronađite proizvod koji želite da naručite</label>
				<br />Iz našeg kataloga proizvoda odaberite proizvod koji želite da kupite.
				Radi bolje pretrage podelili smo naš prodajni asortiman po kategorijama.
				Dodajte proizvode u korpu</p>
				<p><label class="helpn">Dodajte proizvode u korpu </label>
                <br> Dodajte željeni proizvod u svoju korpu klikom na dugme "stavi u korpu" koje se nalazi pored naziva svakog proizvoda u katalogu proizvoda ili u samom opisu proizvoda odmah pored cene proizvoda. Ukoliko želite da kupite više različitih proizvoda 
				  kliknite na link "Nastavi kupovinu" i ponovite postupak sve dok ne dodate sve proizvode koje želite da kupite.</p>
<p><label class="helpn">Naručite proizvode iz korpe </label>
  
 <br> 
 Kada proverite da li u svojoj korpi imate sve proizvode koje želite da kupite, kliknite na dugme "Naruči robu iz korpe".</p>

<p><label class="helpn">Izaberite način plaćanja </label>

<br>Izaberite način plaćanja koji Vam odgovara: Master, Visa American Express kartica. Još jednom proverite sve podatke i klikom na dugme "Potvrdi" Vaša kupovina će biti završena.</p>
<p><label class="helpn">Potvrda narudžbine </label>

<br>Na email koji ste upisali pri prijavi za online kupovinu i koji ćete u budućnosti uvek koristiti pri kupovini ćete dobiti potvrdu o obavljenoj kupovini i na taj način možete biti sigurni da je porudžbina uspešno obavljena i prosleđena do nas. U isto vreme, mi ćemo evidentirati Vašu porudžbinu, odvojiti robu za slanje i poslati Vam. U slučaju da postoji neki problem, neslaganje ili nam je potrebna neka dodatna informacija, kontaktiraćemo Vas prvo na mobilni telefon koji ste ostavili pri registrovaciji, a ako Vas ne nađemo, poslaćemo Vam e-mail.</p>

<p><label class="helpn">Isporuka </label>

<br>Kurir će Vam doneti naručene proizvode na kućnu adresu.</p>
	</div>
			</div>
			</div>
         <?php require_once("footer.php");?>
	</div>
</body>
</html>

