﻿<?php 
//Nenad Rajic
include "userDatabase.php";
session_start();
if(isset($_POST["userName"]) && isset($_POST["password"])){
    $user= new userDatabase();
    $user->loginUser($_POST["userName"], $_POST["password"]);    
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> <style>._css3m{display:none}</style>
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main">
				<div id="login">
				<div id="content">
					<form method="post" action="login.php">
					<label class="login-info">Korisničko ime</label>
					<input class="input-log" name="userName" type="text" />
					<label class="login-info">Lozinka</label>
					<input class="input-log" name="password" type="password" />
						<div id="remember-forgot">
							<div class="checkbox">
								<input name="Checkbox1" type="checkbox" /></div>
							<div class="rememberme">
								Ostavi me prijavljenog</div>
							<div id="forgot-password">
								<a href="#">Zaboravio/la si lozinku ?</a> </div>
							<div id="login-buttton">
								<a href="#" onclick="document.forms[0].submit();return false;"><img src="../images/login_reg/login.png" onmouseover="this.src='../images/login_reg/loginMouseOver.png';" onmouseout="this.src='../images/login_reg/login.png';" /></a>
							</div>
						</div>
					</form>
				</div>
		</div>
			</div>
		</div>
        <?php require_once("footer.php");?>
	</div>
</body>
</html>
