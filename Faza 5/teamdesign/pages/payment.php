﻿<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main">
				<div id="payment">
					<div id="naslov_p">
						<label class="p_naslov">Podaci o platnoj kartici : </label>
					</div>
					<div id="payment_form">
						<table width="480px" height="200px" cellspacing = "0">
							<form method="post">
								<tr>
									<td><label class="pay"> Vrsta kartice :  </label> </td>
									<td class="pay_s">
										<select name = "vrsta">
											<option> Master card </option>
											<option> Visa </option>
											<option> American Express </option>
											<option> Western Union </option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label class="pay"> Broj : </label></td>
									<td><input class="input-p" name="number" type="text" /></td>
								</tr>
								<tr>
									<td><label class="pay"> Kod : </label></td>
									<td><input class="input-p" name="code" type="text" /></td>
								</tr>
								<tr>
									<td><label class="pay"> Datum isteka važenja : </label></td>
									<td class="pay_p">
										Dan: <select>
												<option value="1">01</option>
												<option value="2">02</option>
												<option value="3">03</option>
												<option value="4">04</option>
												<option value="5">05</option>
												<option value="6">06</option>
												<option value="7">07</option>
												<option value="8">08</option>
												<option value="9">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
												<option value="24">24</option>
												<option value="25">25</option>
												<option value="26">26</option>
												<option value="27">27</option>
												<option value="28">28</option>
												<option value="29">29</option>
												<option value="30">30</option>
												<option value="31">31</option>
											</select>
											Mesec: <select>
												<option value="1">01</option>
												<option value="2">02</option>
												<option value="3">03</option>
												<option value="4">04</option>
												<option value="5">05</option>
												<option value="6">06</option>
												<option value="7">07</option>
												<option value="8">08</option>
												<option value="9">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select>
											Godina: <select>
											<option> 2015
											<option> 2016
											<option> 2017
											<option> 2018
											<option> 2019
											</select>
									</td>
								</tr>
								<tr>
									<td><label class="pay"> Ime vlasnika kartice : </label></td>
									<td><input class="input-p" name="p" type="text" /></td>
								</tr>
							</form>
						</table>
					</div>
					<div id="buttons_p">
						<a href="shopping-cart.php"><img src="../images/payment/odustani.png" onmouseover="this.src='../images/payment/odustaniMouseOver.png';" onmouseout="this.src='../images/payment/odustani.png';" /></a>
						<a href="#"><img src="../images/payment/potvrdi.png" onmouseover="this.src='../images/payment/potvrdiMouseOver.png';" onmouseout="this.src='../images/payment/potvrdi.png';" /></a>
					</div>
				</div>
			</div>
			</div>
         <?php require_once("footer.php");?>
	</div>
</body>
</html>
