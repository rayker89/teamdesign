﻿<?php
//Nenad Rajic
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main">
				<div class="contact">	<p>Call Centar
					<br>Telefon: 011-345-543
					<br>Email: prodaja@prodavnicatehnike.rs
					<br>Radno vreme Call Centra: ponedeljak - subota od 8 do 21h; nedelja od 9 do 15h</p>
					<p>Putem Call Centra možete da:
					<br>- izvršite naručivanje robe
					<br>- informišete se o načinima plaćanja, radnom vremenu i uslovima isporuke
					<br>- podnesete prijavu reklamacije telefonskim putem
					<br>- dobijete ostale informacije u vezi kupovine</p>
					<p>Reklamacije
					<br>Email: reklamacije@prodavnicatehnike.rs 
					<br>Telefon: 011-345-544</p>
				</div>
			</div>
         <?php require_once("footer.php");?>
	</div>
</body>
</html>
