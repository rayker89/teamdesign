﻿<?php
//Nenad Rajic
    session_start();
    include "restriction.php";
    include "itemDatabase.php";
    if(isset($_POST["idVesti"])){             
             $item= new itemDatabase();
             $item->removeNews($_POST["idVesti"]);         
             $json = array('success' => true);
             echo json_encode($json);
             exit;
    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/admin_style.css" type="text/css" /> <style>._css3m{display:none}</style>
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
		<div id="pom">
			<div id="header">
			</div> 
			<div id="panel">
				<div id="navigation">
					<div id="nav">
						<table width="700" class="nav">
							<tr>
								<td width="205"> <div align="left"><a href="admin.php">Administratorski meni</div></td>
								<td width="79"> <div align="center"><a href="#">Log Out </a></div></td>
							</tr>
						</table>
					</div>
				</div>
			<div id="menu">
                            <form action="delete-news.php" method="post">
				<div id="option_a">
					<table class="t_option"  width="400px">
						<tr> 
							<td class="naslov" colspan="2"> <a href="#">Ukloni vest</a></td>
						</tr>
						<tr> 
							<td>ID Vesti </td>
							<td><input class="input_admin" name="idVesti" type="text" id="idVesti" /> </td>
						</tr>
					</table>
				</div>
				<div id="buttons">
					&nbsp;&nbsp;&nbsp;<input type="button" value="IZBRISI" id="ukloni" style="width:100px;height:40px"/>
				</div>
                            </form>
			</div>
			</div>
			<div id="footer">
			</div>
		</div>
	</div>
         <script type="text/javascript">
        $(function()
    {
        $("#ukloni").click(function()
        {
            var idVesti = $("#idVesti").val();
            
            

            $.ajax(
            {
                type: "POST",
                dataType: "json",
                url: "delete-news.php",
                data: { idVesti: idVesti },
                success: function(data)
            {
                if(data.success == true){
//                    alert: "success!";
                    window.location.href = 'admin.php';
                }
                
                
            }
            });
            return false;
        });      
    });       
    
</script>
</body>
</html>
