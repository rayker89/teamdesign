﻿<div id="footer">
			<div id="top_footer">
				<label class="navigation_4">
					 <a href="index.php">Home</a> &nbsp; | &nbsp;
					 <a href="pages/news.php">Vesti </a> &nbsp; | &nbsp;
					 <a href="pages/proizvodi.php"> Proizvodi </a> &nbsp; | &nbsp;
				     <a href="pages/action.php">Akcije</a> &nbsp; | &nbsp;
					 <a href="pages/support.php"> Podrška </a>&nbsp; | &nbsp;
					 <a href="pages/aboutus.php"> O Nama </a>&nbsp; | &nbsp;
					 <a href="pages/contact.php"> Kontakt</a>&nbsp;
				</label>
			</div>
			<div id="middle_footer">
				<label class="f_m">
				&nbsp;&nbsp;&nbsp; Sve cene na ovom sajtu iskazane su u dinarima. PDV je uračunat u cenu. Maksimalno koristimo sve svoje resurse da vam svi artikli na ovom sajtu budu prikazani sa ispravnim nazivima specifikacija, fotografijama i cenama.
				Ipak, ne možemo garantovati da su sve navedene informacije i fotografije artikala na ovom sajtu u potpunosti ispravne.
				</label>
			</div>
			<div id="bottom_footer">
			<div id="f_b_l">
				<label class="copyright">
					© Copyright Prodavnica Tehnike. Sva prava zadržana. Developed by <a href="#"> TeamDesign</a>
				</label>
			</div>
			<div id="f_b_c">
				<label class="copyright">Primamo :  </label>
			</div>
			<div id="img_card">
				<img class="img_card" src="images/footer/mastercard.png" alt="MasterCard" height="32" width="32" />
				<img class="img_card" src="images/footer/payment-card.png" alt="PaymentCard" height="32" width="32" />
				<img class="img_card" src="images/footer/AmericanExpress.png" alt="AmericanExpress" height="32" width="32" />
				<img class="img_card" src="images/footer/western_union.png" alt="WesternUnion" height="32" width="32" />
			</div>
			<div id="f_b_r">
				<label class="copyright">Pratite nas na : </label>
			</div>
			<div id="social">
				<a href="facebook.com"><img class="img_card" src="images/footer/facebook.png" alt="facebook" height="32" width="32" /></a>
				<a href="#"><img class="img_card" src="images/footer/twitter.png" alt="twitter" height="32" width="32" /></a>
				<a href="#"><img class="img_card" src="images/footer/GooglePlus.png" alt="GooglePlus" height="32" width="32" /></a>
				<a href = "#"><img class="img_card" src="images/footer/youtube.png" alt="youtube" height="32" width="32" /></a>
			</div>	
		</div>
   	    </div>
