﻿<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main">
			<div class="contact">
				<p>TeamDesign shop je internet prodavnica tehnike i računarske opreme.</p>

<p>Mala ali odabrana TeamDesign ekipa tu je zbog Vas i trudi se da najpovoljnije na tržištu nabavi proizvode iz ponude, direktno od dobavljača i uvoznika i plasira ih vama kao krajnjim korisnicima. 
Trend u svetu, a sve više i kod nas je internet kupovina i elektronsko poslovanje. Mi smo medju prvim prodavnicama ove vrste u Srbiji. Za razliku od većine drugih internet prodavnica nemamo fizičku maloprodaju, veleprodaju i sl. Isključivo poslujemo preko interneta. Iz tog razloga sebe smatramo prvim na tržištu.</p>

<p>Ne postoji niti jedan jedini razlog zbog čega treba izbegavati kupovinu preko Interneta, a hiljadu u korist ovog načina kupovine. Pored onih oficijalnih:</p>
<ul>
<li>mogućnost poručivanja 24h dnevno, sedam dana u nedelji
<li>pristup kompletnom online katalogu proizvoda
<li>potpuna sigurnost i kontrola kupovine
<li>aktuelne informacije o tekućim marketing promocijama
<li>mogućnost samostalnog konfigurisanja računara
</ul>
<p>Kupovinom preko interneta izbegavate i :
čekanja na kasi, 
gužve u radnjama, 
obilazak celog grada kako biste pronašli najpovoljniju cenu 
cimanja u saobraćaju i po raskopanim ulicama u gradu, mogućnost da vas prodavac pošalje u drugu radnju na potpuno drugom kraju grada, jer je baš taj model koji vi želite kod njegovih kolega u drugoj radnji.... <p>
  
 <p> TeamDesign je tu da reši te probleme. Kod nas možete kupovati &bdquo;iz fotelje&ldquo;, pri tom vas niko neće uznemiravati, možete da razgledate koliko god hoćete, bez griže savesti ako na kraju ne kupite i prodavca koji ce konstantno ponavljati &bdquo;izvolite&ldquo;.</p>
 
 <p> DAKLE, KO SMO MI? </p>
  
 <p> Nazovite nas kako hoćete, malom grupom entuzijasta koji se trude da vaš život učine lakšim, skupom velikih profesionalaca koji u svakom trenutku imaju sve što vam treba za početak i razvoj posla, zabavu,... Naš cilj je da putem savremenog načina poslovanja vratimo stare vrednosti. Odluka je pala, vaše je samo da kliknete, jednom, dvaput, triput, i gotovo! Uštedite vreme, živce i novac! Opredelite se za porodicu, prijatelje, familiju, ljubav! Vaše vreme je samo vaše, ostalo je naša briga, uživajte!</p>
</div>
</div>
			</div>
         <?php require_once("footer.php");?>
	</div>
</body>
</html>
