﻿<?php
//Nenad Rajic
include "userDatabase.php";
if(isset($_POST["userName"])){
    $user= new userDatabase();
    $user->registerUser($_POST["userName"],$_POST["password"],$_POST["firstName"],$_POST["lastName"],$_POST["adress"],$_POST["city"],$_POST["email"],$_POST["telephone"]);
    $host = $_SERVER["HTTP_HOST"];
    $path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
    header("Location: http://$host$path/../index.php");
    exit;
    


}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> <style>._css3m{display:none}</style>
<title>Prodavnica Tehnike</title>
 <script type="text/javascript">
    // <![CDATA[

        function validate()
        {
            if (document.forms.registration.email.value == "")
            {
                alert("Morate uneti email adresu.");
                return false;
            }
            else if (document.forms.registration.password.value == "")
            {
                alert("Morate uneti lozinku.");
                return false;
            }
            else if (document.forms.registration.password.value != document.forms.registration.rePassword.value)
            {
                alert("Morate dva puta uneti istu lozinku.");
                return false;
            }
            else if (document.forms.registration.firstName.value == "")
            {
                alert("Morate uneti ime.");
                return false;
            }
            else if (document.forms.registration.lastName.value == "")
            {
                alert("Morate uneti prezime.");
                return false;
            }
            else if (document.forms.registration.telephone.value == "")
            {
                alert("Morate uneti broj telefona.");
                return false;
            }
            else if (document.forms.registration.userName.value == "")
            {
                alert("Morate uneti korisnicko ime.");
                return false;
            }
            
            return true;
        }

    // ]]>
    </script>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("header.php");?>
        
         
		<div id="container">
                     <?php require_once("panelnavigacija.php"); ?>
			<div id="main">
                            
				<div id="registration">
                                    <form action="registration.php" method="post" name="registration" onsubmit="return validate();" >
					<div id="window_reg">
						<table width="350px" height="300px" cellspacing = "0" >
						    
								<tr>
									<td><label class="registration-info"> Ime : </label></td>
									<td><input class="input-reg" name="firstName" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Prezime : <label></td>
									<td><input class="input-reg" name="lastName" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Adresa : </label></td>
									<td><input class="input-reg" name="adress" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Grad : </label></td>
									<td><input class="input-reg" name="city" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Telefon : </label></td>
									<td><input class="input-reg" name="telephone" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">E-Mail : </label></td>
									<td><input class="input-reg" name="email" type="text" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Lozinka: </label></td>
									<td><input class="input-reg" name="password" type="password" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Ponovite Lozinku: </label></td>
									<td><input class="input-reg" name="rePassword" type="password" /></td>
								</tr>
								<tr>
									<td><label class="registration-info">Korisničko ime : </label></td>
									<td><input class="input-reg" name="userName" type="text" /></td>
								</tr>
								<tr>
									<td colspan="2"><input name="agreement" type="checkbox" onclick="document.forms.registration.button.disabled = !document.forms.registration.button.disabled;"/> <label class="link"><a href="TermOfUse.php" > Prihvatam uslove korišćenja</a></label></td>
								</tr>
								<tr>
									
							
						</table>
					</div>                                        
					<div id="buttons_r">
                                                &nbsp;&nbsp;&nbsp;<input type="reset" value="RESETUJ" style="width:100px;height:40px"/>
                                                &nbsp;&nbsp;&nbsp;<input type="submit" disabled="disabled" name="button" value="POTVRDI" style="width:100px;height:40px"/>
					</div>
                                    </form>
				</div>
			</div>
		</div>
        <?php require_once("footer.php");?>
	</div>
</body>
</html>
