﻿<?php
//Nenad Rajic
    session_start();
    include "restriction.php";
    if(isset($_POST["select-kategorija"])){    
        include "itemDatabase.php";
        $item= new itemDatabase();       
        $item->addItem($_POST["naziv"], $_POST["select-kategorija"], $_POST["select-manufacturer"], $_POST["kolicina"], $_POST["cena"], $_POST["opis"], $_FILES);        
        $host = $_SERVER["HTTP_HOST"];
        $path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
        header("Location: http://$host$path/admin.php");
        exit;
        
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/admin_style.css" type="text/css" /> <style>._css3m{display:none}</style>
<title>Prodavnica Tehnike</title>
<script type="text/javascript">
    // <![CDATA[

        function validate()
        {
            if (document.forms.addproduct.naziv.value == "")
            {
                alert("Morate uneti naziv komponente.");
                return false;
            }
            else if (document.forms.addproduct.kolicina.value == "")
            {
                alert("Morate uneti kolicinu.");
                return false;
            }
            else if (document.forms.addproduct.cena.value =="")
            {
                alert("Morate uneti cenu komponente.");
                return false;
            }
            else if (document.forms.addproduct.opis.value == "")
            {
                alert("Morate uneti opis komponente.");
                return false;
            }            
            
            return true;
        }

    // ]]>
    </script>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
		<div id="pom">
			<div id="header">
			</div> 
			<div id="panel">
				<div id="navigation">
					<div id="nav">
						<table width="700" class="nav">
							<tr>
								<td width="205"> <div align="left"><a href="admin.php">Administratorski meni</div></td>
								<td width="79"> <div align="center"><a href="#">Log Out </a></div></td>
							</tr>
						</table>
					</div>
				</div>
			<div id="menu">
                            <form action="add-product.php" method="post" enctype="multipart/form-data" name="addproduct" onsubmit="return validate();">
				<div id="option_a">
					<table class="t_option"  width="400px">
						<tr> 
							<td class="naslov" colspan="2"> <a href="#">Dodavanje artikla</a></td>
						</tr>
						<tr> 
							<td>Naziv artikla</td>
							<td> <input class="input_admin" name="naziv" type="text" /></td>
						</tr>
						<tr> 
							<td>  Kategorija artikla</td>
							<td class="s">  <select name="select-kategorija">
									<option>Fotoaparati</option>
									<option>Frizideri</option>
                                                                        <option>GrafickeKarte</option>
									<option>HardDiskovi</option>
                                                                        <option>HDDRack</option>
									<option>Kamere</option>
                                                                        <option>Klime</option>
									<option>Kuleri</option>
                                                                        <option>Laptopovi</option>
									<option>MasineZaSudove</option>
                                                                        <option>MasineZaVes</option>
									<option>MaticnePloce</option>
                                                                        <option>Memorija</option>
									<option>MikrotalasneRerne</option>
                                                                        <option>Misevi</option>
									<option>MobilniTelefoni</option>
                                                                        <option>Monitori</option>
                                                                        <option>OstalaOprema</option>
									<option>Sporeti</option>
									<option>Tableti</option>
									<option>Televizori</option>
									<option>Zvucnici</option>
                                                            </select>
									</td>
						</tr>
						<tr> 
							<td>  Proizvođač</td>
							<td class="s">  <select name="select-manufacturer">
								  <option>Apple</option>
								  <option>Altec</option>
								  <option>AMD</option>
								  <option>Bira</option>
								  <option>Canon</option>
								  <option>Gorenje</option>
								  <option>Intel</option>
								  <option>LG</option>
								  <option>Logitech</option>
                                                                  <option>Maxtor</option>
								  <option>MSI</option>
								  <option>Nvidia</option>
                                                                  <option>Samsung</option>
								  <option>Termaltake</option>
                                                                  <option>Western Digital</option>
                                                                  
                                                            </select>
								  </td>
						</tr>
						<tr> 
							<td>Količina </td>
							<td><input class="input_admin" name="kolicina" type="text" /> </td>
						</tr>
						<tr> 
							<td>Cena </td>
							<td><input class="input_admin" name="cena" type="text" /> </td>
						</tr>
						<tr> 
							<td>Slika artikla</td>
                                                        <td><input type="file" name="file"  size="40"></input></td>
						</tr>
						<tr> 
							<td>Opis</td>
							<td class="s"><textarea name="opis" rows="5" cols="30"></textarea></td>
						</tr>
					</table>
				</div>
				<div id="buttons">
					 &nbsp;&nbsp;&nbsp;<input type="reset" value="RESETUJ" style="width:100px;height:40px"/>
                                         &nbsp;&nbsp;&nbsp;<input type="submit" name="button" value="POTVRDI" style="width:100px;height:40px"/>
				</div>
                            </form>
			</div>
			</div>
			<div id="footer">
			</div>
		</div>
	</div>
 
</body>
</html>
