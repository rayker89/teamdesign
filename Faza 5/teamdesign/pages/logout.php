﻿<?php
//Nenad Rajic
session_start();
$_SESSION = array();
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finally, destroy the session.
session_destroy();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/style.css" type="text/css" /> 
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
    	<?php require_once("Header.php");?>
		<div id="container">
			<?php require_once("panelnavigacija.php");?>
			<div id="main">
				<?php echo "<p style='color:white;'>" . "Uspešno ste se izlogovali". "</p>" ?>
			</div>
			</div>
         <?php require_once("Footer.php");?>
	</div>
</body>
</html>