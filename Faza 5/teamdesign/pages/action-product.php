﻿<?php 
    //Stefan Šomođi
    session_start();
    include "restriction.php";
    include "itemDatabase.php";
    if(isset($_POST["itemid"])){
        if(isset($_POST["percentage"])){        
             $item= new itemDatabase();
             $item->addAction($_POST["itemid"],$_POST["percentage"]); 
            
        }else{
            $item=new itemDatabase();
            $item->removeAction($_POST["itemid"]);
            
        }
        $json = array('success' => true);
        echo json_encode($json);
        exit;

    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/admin_style.css" type="text/css" /> <style>._css3m{display:none}</style>
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<title>Prodavnica Tehnike</title>
</head>
<!-- Valentina Prcovic -->
<body>
	<div id="wraper">
		<div id="pom">
			<div id="header">
			</div> 
			<div id="panel">
				<div id="navigation">
					<div id="nav">
						<table width="700" class="nav">
							<tr>
								<td width="205"> <div align="left"><a href="admin.php">Administratorski meni</div></td>								
								<td width="79"> <div align="center"><a href="#">Log Out </a></div></td>
							</tr>
						</table>
					</div>
				</div>
			<div id="menu">
				<div id="option_a">
					<table class="t_option"  width="400px">
						<tr> 
							<td class="naslov" colspan="2"> <a href="#">Dodavanje artikla na akciju</a></td>
						</tr>
						<tr> 
							<td>Unesite šifru artikla</td>
							<td> <input class="input_admin" name="itemid" type="text" id="itemid" /></td>
						</tr>
						<tr> 
							<td>Broj procenata</td>
							<td> <input class="input_admin" name="percentage" type="text" id="percentage" /></td>
						</tr>
					</table>
				</div>
				<div id="buttons">
					&nbsp;&nbsp;&nbsp;<input type="button" value="DODAJ" id="dodaj" style="width:100px;height:40px"/>
                                        &nbsp;&nbsp;&nbsp;<input type="button" value="UKLONI" id="ukloni" style="width:100px;height:40px"/>
				</div>
			</div>
			</div>
			<div id="footer">
			</div>
		</div>
	</div>
    <script type="text/javascript">
        $(function()
    {
        $("#ukloni").click(function()
        {
            var itemid = $("#itemid").val();
            

            $.ajax(
            {
                type: "POST",
                dataType: "json",
                url: "action-product.php",
                data: { itemid: itemid  },
                success: function(data)
            {
                if(data.success == true){
//                    alert: "success!";
                    window.location.href = 'admin.php';
                }
                
                
            }
            });
            return false;
        });
        $("#dodaj").click(function()
        {
             var itemid = $("#itemid").val();
             var percentage= $("#percentage").val();

            $.ajax(
            {
                type: "POST",
                dataType: "json",
                url: "action-product.php",
                data: { itemid: itemid , percentage: percentage },
                success: function(data)
            {
                if(data.success == true){
//                    alert: "success!";
                    window.location.href = 'admin.php';
                }
                
                
            }
            });
            return false;
        });
    });       
    
</script>
</body>
</html>
